import 'package:flutter/material.dart';

import 'package:drinker/ui/home/tab_home.dart';
import 'package:drinker/ui/product/tab_product.dart';
import 'package:drinker/ui/business/tab_business.dart';
import 'package:drinker/ui/map/tab_map.dart';

class AppTab extends StatefulWidget {
  @override
  _AppTabState createState() => _AppTabState();
}

// SingleTickerProviderStateMixin is used for animation
class _AppTabState extends State<AppTab> with SingleTickerProviderStateMixin {
  int _currentIndex = 0;

  TabController _controller;

  List<Widget> listView = [
    new TabHome(),
    new TabProduct(),
    new TabBusiness(),
    new TabMap()
  ];

  _onChanged() {
    setState(() {
      _currentIndex = this._controller.index;
    });
  }

  @override
  void initState() {
    super.initState();

    _controller = new TabController(initialIndex: _currentIndex, length: listView.length, vsync: this);
    
    _controller.addListener(_onChanged);
  }

  @override
  void dispose() {
    // Dispose of the Tab Controller
    _controller.removeListener(_onChanged);
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TabBarView(
        controller: _controller,
        physics: NeverScrollableScrollPhysics(),
        children: listView
      ),
      bottomNavigationBar: Container(
        height: 40,
        color: Theme.of(context).bottomAppBarTheme.color,
        child: TabBar(
          controller: _controller,
          tabs: <Widget>[
            Tab(
              icon: Image(
                image: _currentIndex == 0 ? AssetImage('assets/icons/ic_tab_home_selected.png') : AssetImage('assets/icons/ic_tab_home.png')
              ),
            ),
            Tab(
              icon: Image(
                image: _currentIndex == 1 ? AssetImage('assets/icons/ic_tab_product_selected.png') : AssetImage('assets/icons/ic_tab_product.png')
              ),
            ),
            Tab(
              icon: Image(
                image: _currentIndex == 2 ? AssetImage('assets/icons/ic_tab_business_selected.png') : AssetImage('assets/icons/ic_tab_business.png')
              ),
            ),
            Tab(
              icon: Image(
                image: _currentIndex == 3 ? AssetImage('assets/icons/ic_tab_map_selected.png') : AssetImage('assets/icons/ic_tab_map.png')
              ),
            ),
          ],
          indicatorColor: Theme.of(context).indicatorColor,
        ),
      )
    );
  }
}
import 'dart:async';
import 'dart:convert';
import 'package:drinker/model/Business.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;

import 'package:drinker/model/Map.dart';
import 'package:drinker/shared/app_bar.dart';

class TabMap extends StatefulWidget {
  final Business business;

  TabMap({ this.business });

  @override
  _TabMapState createState() => _TabMapState();
}

class _TabMapState extends State<TabMap> {
  CameraPosition _positionInit = new CameraPosition(target: LatLng(0, 0), zoom: 16);
  GoogleMapController _mapController;
  TextEditingController _textEditingController;

  Location _location = new Location();
  LocationData _userLocation;
  List<MapBusiness> items = List<MapBusiness>();
  final Set<Marker> _markers = {};
  bool _isLoading = true;
  bool _hasPermission = false;
  MapBusiness _selectedBusiness;

  Future<void> _getCurrentLocation() async {
    if (widget.business == null) {
      LocationData currentLocation;
      try {
        currentLocation = await _location.getLocation();
        setState(() {
          _userLocation = currentLocation;
          _positionInit = new CameraPosition(target: LatLng(_userLocation.latitude, _userLocation.longitude), zoom: 16);
          _isLoading = false;
        });
      } catch (e) {
        print(e);
      }
    }
    
  }

  Future _getPermission() async {
    var requestService = await _location.requestService();
    if (requestService) {
      _getCurrentLocation();
      setState(() {
        _hasPermission = true;
      });
    } else {
      setState(() {
        _hasPermission = false;
      });
    }
  }

  void _addMarker(MapBusiness business) async {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    ImageConfiguration imageConfig = ImageConfiguration(devicePixelRatio: mediaQueryData.devicePixelRatio);
    final MarkerId markerId = MarkerId(business.id);
    var marker;
    marker = Marker(
      markerId: markerId,
      position: LatLng(business.geoLocation.coordinates[0]?.toDouble() ?? 0.0, business.geoLocation.coordinates[1]?.toDouble() ?? 0.0),
      infoWindow: InfoWindow(title: business.name, snippet: business.address),
      icon: await BitmapDescriptor.fromAssetImage(imageConfig, 'assets/icons/ic_point_map.png'),
    );

    setState(() {
      _markers.add(marker);
    });
  }

  Future<void> _getLocationInit() async {
    if (widget.business != null) {
      setState(() {
        _positionInit = new CameraPosition(target: LatLng(_selectedBusiness.geoLocation.coordinates[0]?.toDouble() ?? 0.0, _selectedBusiness.geoLocation.coordinates[1]?.toDouble() ?? 0.0), zoom: 16);
        _isLoading = false;
      });
    }
  }

  Future<void> _getLocationUser() async {
    if (await _location.hasPermission()) {
      _mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(_userLocation.latitude, _userLocation.longitude), zoom: 16)));
      if (_selectedBusiness != null) {
        _textEditingController.text = '';
        setState(() {
          _selectedBusiness = new MapBusiness();
        });
      }
    }
  }

  Future<void> _getLocationBusiness() async {
    if (await _location.hasPermission()) {
      _mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(_selectedBusiness.geoLocation.coordinates[0]?.toDouble() ?? 0.0, _selectedBusiness.geoLocation.coordinates[1]?.toDouble() ?? 0.0), zoom: 16)));
    }
  }

  void _fetchBusiness() async { 
    final response = await http.get('https://prod-backend-dot-platform-drinker.appspot.com/api/v1/shared/business'); 
    if (response.statusCode == 200) { 
      Map<String, dynamic> resp = jsonDecode(response.body);
      final List<MapBusiness> business = resp['data'].map((item) => MapBusiness.fromJson(item)).cast<MapBusiness>().toList();

      setState(() {
        items = business;
        items.forEach((item) => _addMarker(item));
        
      });
      if (widget.business != null) {
        setState(() {
          _selectedBusiness = items.firstWhere((item) => item.id.toLowerCase().contains(widget.business.id), orElse: () => null);
          _getLocationInit();
        });
      }
    } else { 
        throw Exception('Unable to fetch products from the REST API'); 
    }
  }

  Widget _showProgress() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CircularProgressIndicator(),
          Text('Por favor espere, ubicando...')
        ],
      ),
    );
  }

  _filterSearch(String query) {
    List<MapBusiness> dummyListData;

    if (query.isNotEmpty) {
      dummyListData = items
                        .where((item) => item.name.toLowerCase().contains(query.toLowerCase()))
                        .toList();
    } else {
      dummyListData = items.toList();
    }

    return dummyListData;
  }

  Widget _showMap() {
    return Stack(
      children: <Widget>[
        GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: _positionInit,
          myLocationEnabled: true,
          myLocationButtonEnabled: false,
          markers: _markers,
        ),
        Positioned(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: TypeAheadFormField(
              textFieldConfiguration: TextFieldConfiguration(
                controller: this._textEditingController,
                decoration: InputDecoration(
                  hintText: 'Buscar',
                  prefixIcon: Icon(Icons.search, color: Theme.of(context).inputDecorationTheme.hintStyle.color),
                  contentPadding: EdgeInsets.all(5.0),
                ),
                style: Theme.of(context).inputDecorationTheme.hintStyle,
              ),
              suggestionsCallback: (pattern) {
                return _filterSearch(pattern);
              },
              itemBuilder: (context, business) {
                return ListTile(
                  title: Text(
                    business.name, 
                    style: TextStyle(color: Colors.black),
                  ),
                  contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                );
              },
              transitionBuilder: (context, suggestionsBox, controller) {
                return suggestionsBox;
              },
              onSuggestionSelected: (business) {
                _textEditingController.text = business.name;
                setState(() {
                  _selectedBusiness = business;
                });
                
                _getLocationBusiness();
              },
              validator: (value) {
                if (value.isEmpty) {
                  return 'Por favor, seleccione un local.';
                }
              },
              onSaved: (value) => print('on saved $value'),

            ),
          )
        ),
      ],
    );
  }

  Widget _notPermission() {
    return Container(
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              child: Text(
                'Por favor, permita obtener la ubicación del dispositivo.',
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 32.0,
            ),
            FlatButton(
              onPressed: () {
                _getPermission();
              },
              child: Text(
                'Reintentar',
              )
            )
          ],
        ),
      ),
    );
  }
  
  void initState() {
    super.initState();
     _textEditingController = TextEditingController();

    _getPermission();

    _fetchBusiness();

  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  void _onMapCreated(GoogleMapController controller) {
    _mapController = controller;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBarCustom(
        title: 'Mapa',
      ),
      body: Builder(
        builder: (context) => _isLoading
          ? _showProgress()
          : _hasPermission
            ? _showMap()
            : _notPermission()
      ),
      floatingActionButton: _isLoading
        ? null
        : FloatingActionButton(
            backgroundColor: Colors.white,
            foregroundColor: Color.fromRGBO(204, 102, 71, 1),
            mini: true,
            child: Icon(Icons.gps_fixed),
            onPressed: _getLocationUser,
          ),
    );
  }
}
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:drinker/helper/helper.dart';
import 'package:drinker/model/Business.dart';
import 'package:drinker/model/ProductDetail.dart';
import 'package:drinker/service/firebase_firestore_service.dart';

class BusinessDetail extends StatefulWidget {
  final Business business;
  BusinessDetail(this.business);

  @override
  _BusinessDetailState createState() => _BusinessDetailState();
}

class _BusinessDetailState extends State<BusinessDetail> {
  List<ProductDeatail> products;
  FirebaseFirestoreService _db;
  StreamSubscription<QuerySnapshot> productSub;

  _navigateTo(BuildContext context) async {
    await Navigator.pushNamed(
      context, 
      '/map/business', 
      arguments: widget.business
    );
  }

  @override
  void initState() {
    super.initState();
    _db = new FirebaseFirestoreService('feed_business/${widget.business.id}/products');
    products = new List();
    productSub?.cancel();
    productSub = _db.getCollection(orderBy: 'price').listen((QuerySnapshot snapshot) {
      final List<ProductDeatail> lists = snapshot.documents.map((documentSnapshot) => ProductDeatail.fromMap(documentSnapshot.data)).toList();
   
      setState(() {
        this.products = lists;
      });
    });
  }

  @override
  void dispose() {
    productSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "${widget.business.name}",
          style: Theme.of(context).textTheme.title
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
        flexibleSpace: Container(
          height: 90,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(194, 54, 65, 1),
                Color.fromRGBO(204, 102, 71, 1),
                Color.fromRGBO(213, 134, 64, 1)
              ]
            ),
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 15),
            child: InkWell(
              onTap: () => _navigateTo(context),
              child: Icon(Icons.place),
            ),
          ),
        ],
      ),
      body: Center(
        child: ListView.builder(
          itemCount: products.length,
          itemBuilder: (context, position) {
            return new InkWell(
              onTap: () => _navigateTo(context),
              child: Card(
                color: position % 2 == 0 ? Colors.white : Color.fromARGB(255, 255, 216, 204),
                margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: 120,
                        child: Text(
                          "${products[position].name}",
                          style: Theme.of(context).accentTextTheme.body1,
                        ),
                      ),
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            Center(
                              child: Text(
                                "${products[position].presentation}",
                                style: Theme.of(context).accentTextTheme.body1,
                              ),
                            )
                          ],
                        ),
                      ),
                      Text(
                        "\$ ${HelperFormatNumber.currency?.format(products[position].price)}",
                        style: Theme.of(context).accentTextTheme.body2,
                      )
                    ],
                  )
                )
              ),
            );
          },
        ),
      )
    );
  }
}

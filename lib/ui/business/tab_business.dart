import 'dart:async';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:drinker/shared/app_bar.dart';
import 'package:drinker/service/firebase_firestore_service.dart';
import 'package:drinker/model/Business.dart';
import 'package:drinker/model/FeedBusiness.dart';

class TabBusiness extends StatefulWidget {
  @override
  _TabBusinessState createState() => new _TabBusinessState();
}

class _TabBusinessState extends State<TabBusiness> {
  final editingController = TextEditingController();
  List<FeedBusiness> _items, _listOrigin;
  FirebaseFirestoreService db = new FirebaseFirestoreService("feed_business");

  StreamSubscription<QuerySnapshot> businessSub;

  _navigateTo(BuildContext context, Business business) async {
    await Navigator.pushNamed(
      context, 
      '/business/detail', 
      arguments: business
    );
  }

  @override
  void initState() {
    super.initState();

    _items = new List();
    businessSub = db.getCollection().listen((QuerySnapshot snapshot) {
      final List<FeedBusiness> lists = snapshot.documents.map((documentSnapshot) {
        return FeedBusiness.fromMap(documentSnapshot.data);
      }).toList();
   
      setState(() {
        _items = lists;
        _listOrigin = List.from(_items);
      });
    });

    editingController.addListener(() {
      _filterSearch(editingController.text);
    });
  }

  @override
  void dispose() {
    editingController.dispose();
    businessSub?.cancel();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBarCustom(title: 'Locales'),
      body: Container(
        child: Column(
          children: <Widget>[
            appTextField(),
            Expanded(
              child: ListView.builder(
                itemCount: _items.length,
                itemBuilder: (context, position) {
                  return Card(
                    color: position % 2 == 0 ? Colors.white : Color.fromARGB(255, 255, 216, 204),
                    elevation: 4,
                    margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: GestureDetector(
                              onTap: () => _navigateTo(context, _items[position].business),
                              child: Text('${_items[position].name}',
                                textAlign: TextAlign.center,
                                style: Theme.of(context).accentTextTheme.body1,
                              ),
                            ),
                          )
                        ]
                      )
                    )
                  );
                },
              )
            )
          ],
        )
      )
    );
  }

  

  appTextField() {
    return Padding(
      padding: EdgeInsets.fromLTRB(5, 10, 5, 0),
      child: TextField(
        controller: editingController,
        decoration: InputDecoration(
          hintText: 'Buscar',
          prefixIcon: Icon(Icons.search, color: Theme.of(context).inputDecorationTheme.hintStyle.color),
          contentPadding: EdgeInsets.all(5.0),
        ),
        style: Theme.of(context).inputDecorationTheme.hintStyle
      ),
    );
  }

  _filterSearch(String query) {
    List<FeedBusiness> dummyListData;

    if (query.isNotEmpty) {
      dummyListData = _listOrigin
          .where((item) =>
              item.name.toLowerCase().contains(query.toLowerCase()))
          .toList();
    } else {
      dummyListData = _listOrigin.toList();
    }
    setState(() {
      _items.clear();
      _items.addAll(dummyListData);
    });
  }
}


import 'dart:async';
import 'package:drinker/model/Business.dart';
import 'package:drinker/model/product.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:drinker/helper/helper.dart';

import 'package:drinker/shared/app_bar.dart';

import 'package:drinker/service/firebase_firestore_service.dart';
import 'package:drinker/model/FeedHome.dart';

class TabHome extends StatefulWidget {
  @override
  _TabHomeState createState() => _TabHomeState();
}

class _TabHomeState extends State<TabHome> {
  final editingController = TextEditingController();
  List<FeedHome> _items = List<FeedHome>(), _listOrigin;
  FirebaseFirestoreService db = FirebaseFirestoreService("feed_home");

  StreamSubscription<QuerySnapshot> homeSub;

  _navigateToProduct(BuildContext context, Product product) async {
    await Navigator.pushNamed(
      context, 
      '/product/detail', 
      arguments: product
    );
  }

  _navigateToBusiness(BuildContext context, Business business) async {
    await Navigator.pushNamed(
      context,
      '/business/detail',
      arguments: business
    );
  }

  @override
  void initState() {
    super.initState();

    homeSub = getList();

    editingController.addListener(() {
      _filterSearch(editingController.text);
    });
  }

  @override
  void dispose() {
    homeSub?.cancel();
    editingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarCustom(
        title: 'Promedio',
        hasSubTitle: true,
        subTitle: '\$ ${HelperFormatNumber.currency.format(avg())}',
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            appTextField(),
            Expanded(
              child: ListView.builder(
                itemCount: _items.length,
                itemBuilder: (context, position) {
                  return Card(
                    color: position % 2 == 0 ? Colors.white : Color.fromARGB(255, 255, 216, 204),
                    margin: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            width: 100,
                            child: InkWell(
                              onTap: () {
                                _navigateToProduct(
                                  context,
                                  Product(
                                    id: _items[position].product.id,
                                    name: _items[position].product.name,
                                    presentation: _items[position].presentation,
                                    brand: _items[position].brand.name
                                  )
                                );
                              },
                              child: Column(
                                children: <Widget>[
                                  Text('${_items[position].productName}'),
                                  Container(
                                    margin: EdgeInsets.only(top: 3),
                                    child: Text(
                                      '${_items[position].presentation}',
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                        fontSize: 10
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                _navigateToBusiness(
                                  context,
                                  _items[position].business
                                );
                              },
                              child: Center(
                                child: Text('${_items[position].business.name}'),
                              ),
                            )
                          ),
                          Row(
                            children: <Widget>[
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    '\$ ${HelperFormatNumber.currency?.format(_items[position].price)}',
                                    style: Theme.of(context).textTheme.body2,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 3),
                                    child: Text(
                                      '${_dateDifference(_items[position].updatedAt)}',
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                        fontSize: 10
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 2.0),
                                child: getIcon(_items[position].place, _items[position].oldPlace),
                              ),
                            ],
                          )
                        ],
                      )
                    )
                  );
                },
              ),
            )
          ],
        )
      )
    );
  }

  double avg() {
    return _items.fold(0, (prev, element) => prev + (element.price ?? 0) ) / (_items.length != 0 ? _items.length : 1);
  }
      

  getIcon(int place, int oldPlace) {
    if (place < oldPlace) {
      return Icon(
        Icons.expand_less,
        color: Colors.green,
        size: 20,
      );
    } else if (place > oldPlace) {
      return Icon(
        Icons.expand_more,
        color: Colors.red,
        size: 20,
      );
    }
  }

  StreamSubscription<QuerySnapshot> getList() {
    return db
        .getCollection(orderBy: 'price')
        .listen((QuerySnapshot snapshot) {
      final List<FeedHome> lists = snapshot.documents
          .map((documentSnapshot) => FeedHome.fromMap(documentSnapshot.data))
          .toList();

      setState(() {
        _items = lists;
        _listOrigin = List.from(_items);
      });
    });
  }

  appTextField() {
    return Padding(
      padding: EdgeInsets.fromLTRB(5, 10, 5, 0),
      child: TextField(
        controller: editingController,
        decoration: InputDecoration(
          hintText: 'Buscar',
          prefixIcon: Icon(Icons.search, color: Theme.of(context).inputDecorationTheme.hintStyle.color),
          contentPadding: EdgeInsets.all(5.0),
        ),
        style: Theme.of(context).inputDecorationTheme.hintStyle
      ),
    );
  }

  _filterSearch(String query) {
    List<FeedHome> dummyListData;

    if (query.isNotEmpty) {
      dummyListData = _listOrigin
          .where((item) =>
              item.productName.toLowerCase().contains(query.toLowerCase()) ||
              item.business.name.toLowerCase().contains(query.toLowerCase()))
          .toList();
    } else {
      dummyListData = _listOrigin.toList();
    }
    setState(() {
      _items.clear();
      _items.addAll(dummyListData);
    });
  }

  _dateDifference(DateTime date) {
    var _now = new DateTime.now();
    final _difference =  _now.difference(date);

    if (_difference < new Duration(seconds: 60)) {
      return '${_difference.inSeconds} s.';
    }
    if (_difference >= new Duration(seconds: 60) && _difference < new Duration(minutes: 60)) {
      return '${_difference.inMinutes} m.';
    }
    if (_difference >= new Duration(minutes: 60) && _difference < new Duration(hours: 24)) {
      return '${_difference.inHours} h.';
    }
    if (_difference >= new Duration(hours: 24) && _difference < new Duration(days: 365)) {
      return '${_difference.inDays} d.';
    }
    return '> a';
  }
}

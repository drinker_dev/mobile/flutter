import 'dart:async';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:drinker/helper/helper.dart';

import 'package:drinker/service/firebase_firestore_service.dart';

import 'package:drinker/model/FeedProduct.dart';
import 'package:drinker/model/product.dart';
import 'package:drinker/shared/app_bar.dart';

class TabProduct extends StatefulWidget {
  @override
  _TabProduct createState() => new _TabProduct();
}

class _TabProduct extends State<TabProduct> {
  final editingController = TextEditingController();
  List<FeedProduct> _items, _listOrigin;
  FirebaseFirestoreService db = new FirebaseFirestoreService("feed_products");

  StreamSubscription<QuerySnapshot> productSub;

  _navigateTo(BuildContext context, Product product) async {
    await Navigator.pushNamed(
      context, 
      '/product/detail', 
      arguments: product
    );
  }

  @override
  void initState() {
    super.initState();

    _items = new List();
    productSub?.cancel();
    productSub = db.getCollection(orderBy: 'average').listen((QuerySnapshot snapshot) {
      final List<FeedProduct> lists = snapshot.documents.map((documentSnapshot) => FeedProduct.fromMap(documentSnapshot.data)).toList();
   
      setState(() {
        _items = lists;
        _listOrigin = List.from(_items);
      });
    });

    editingController.addListener(() {
      _filterSearch(editingController.text);
    });
  }

  @override
  void dispose() {
    editingController.dispose();
    productSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBarCustom(title: 'Cervezas'),
      body: Container(
        child: Column(
          children: <Widget>[
            appTextField(),
            Expanded(
              child: ListView.builder(
                itemCount: _items.length,
                itemBuilder: (context, position) {
                  return new InkWell(
                    onTap: () => _navigateTo(
                      context,
                      Product(
                        id: _items[position].id,
                        name: _items[position].product.name,
                        presentation: _items[position].presentation,
                        brand: _items[position].brand.name
                      )
                    ),
                    child: new Card(
                      color: position % 2 == 0 ? Colors.white : Color.fromARGB(255, 255, 216, 204),
                      elevation: 4,
                      margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                        child: new Row(
                          children: <Widget>[
                            Container(
                              width: 120,
                              child: Text(
                                '${_items[position].name}',
                                style: Theme.of(context).accentTextTheme.body1,
                              )
                            ),
                            Expanded(
                              child: Center(
                                child: Text('${_items[position].presentation}'),
                              ),
                            ),
                            Container(
                              width: 90,
                              child: Text(
                                '\$ ${HelperFormatNumber.currency.format(_items[position].average)}',
                                textAlign: TextAlign.right,
                                style: Theme.of(context).accentTextTheme.body2,
                              ),
                            ),
                          ],
                        ),
                      )
                    ),
                  );
                }
              )
            )
          ],
        )
      )
    );
  }

  appTextField() {
    return Padding(
      padding: EdgeInsets.fromLTRB(5, 10, 5, 0),
      child: TextField(
        controller: editingController,
        decoration: InputDecoration(
          hintText: 'Buscar',
          prefixIcon: Icon(Icons.search, color: Theme.of(context).inputDecorationTheme.hintStyle.color),
          contentPadding: EdgeInsets.all(5.0),
        ),
        style: Theme.of(context).inputDecorationTheme.hintStyle
      ),
    );
  }

  _filterSearch(String query) {
    List<FeedProduct> dummyListData;


    if (query.isNotEmpty) {
      dummyListData = _listOrigin.where((item) =>
        item.name.toLowerCase().contains(query.toLowerCase())).toList();
    } else {
      dummyListData = _listOrigin.toList();
    }
    setState(() {
      _items.clear();
      _items.addAll(dummyListData);
    });
  }
}

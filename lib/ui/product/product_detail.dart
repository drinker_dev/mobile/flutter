import 'dart:async';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:drinker/model/BusinessDetail.dart';
import 'package:drinker/helper/helper.dart';
import 'package:drinker/model/product.dart';
import 'package:drinker/service/firebase_firestore_service.dart';

class ProductDetail extends StatefulWidget {
  final Product product;
  ProductDetail(this.product);

  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  List<BusinessDetail> business;
  FirebaseFirestoreService _db;
  StreamSubscription<QuerySnapshot> businessSub;

  @override
  void initState() {
    super.initState();
    _db = new FirebaseFirestoreService('feed_products/${widget.product.id}_${widget.product.presentation}/business');
    business = new List();
    businessSub = _db.getCollection(orderBy: 'price').listen((QuerySnapshot snapshot) {
      final List<BusinessDetail> lists = snapshot.documents.map((documentSnapshot) => BusinessDetail.fromMap(documentSnapshot.data)).toList();
   
      setState(() {
        this.business = lists;
      });
    });
  }

  @override
  void dispose() {
    businessSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "${widget.product.nameComplete}",
          style: Theme.of(context).textTheme.title
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
        flexibleSpace: Container(
          height: 90,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(194, 54, 65, 1),
                Color.fromRGBO(204, 102, 71, 1),
                Color.fromRGBO(213, 134, 64, 1)
              ]
            ),
          ),
        ),
      ),
      body: Center(
        child: ListView.builder(
          itemCount: business.length,
          itemBuilder: (context, position) {
            return Card(
              color: position % 2 == 0 ? Colors.white : Color.fromARGB(255, 255, 216, 204),
              margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 130,
                      child: Text(
                        "${business[position].name}",
                        style: Theme.of(context).accentTextTheme.body1,
                      )
                    ),
                    Text(
                      "${widget.product.presentation}",
                      style: Theme.of(context).accentTextTheme.body1,
                    ),
                    Text(
                      "\$ ${HelperFormatNumber.currency.format(business[position].price)}",
                        style: Theme.of(context).accentTextTheme.body2,
                      )
                  ],
                )
              )
            );
          },
        ),
      )
    );
  }
}

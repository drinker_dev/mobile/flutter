import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
 
class FirebaseFirestoreService {

  String _collection;
  final _intance = Firestore.instance;

  FirebaseFirestoreService(this._collection);

  Stream<QuerySnapshot> getCollection({String orderBy = '', int offset, int limit}) {
    Query query = _intance.collection(_collection);

    if (orderBy != '') {
      query = query.orderBy(orderBy);
    } 

    Stream<QuerySnapshot> snapshots = query.snapshots();
 
    if (offset != null) {
      snapshots = snapshots.skip(offset);
    }
 
    if (limit != null) {
      snapshots = snapshots.take(limit);
    }
    return snapshots;
  }
}
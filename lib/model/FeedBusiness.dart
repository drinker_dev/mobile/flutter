import 'package:drinker/model/Business.dart';

class FeedBusiness {
  Business _business;
 
  Business get business => _business;
  String get id => _business.id;
  String get name => _business.name;
 
  FeedBusiness.fromMap(Map<dynamic, dynamic> map) {
    this._business = Business.fromMap(map['business']);
  }

  FeedBusiness.avg(List prices) {
    prices.fold(0, (prev, element) => prev + element);
  }

  Map<String, dynamic> toJson() => {
    'business': business.toJson(),
  };
}
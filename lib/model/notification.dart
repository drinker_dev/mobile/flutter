class Notification {
  String _table = 'notification';
  int _id;
  String _title;
  String _body;
  bool _status;

  Notification(this._title, this._body, [this._status = false]);

  String get table => _table;
  int get id => _id;
  String get title => _title;
  String get body => _body;
  bool get status => _status;

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'title': _title,
      'body': _body
    };
    if (_id != null) {
      map['id'] = _id;
    }
    return map;
  }

  Notification.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._title = map['title'];
    this._body = map['body'];
    this._status = map['status'];
  }
}
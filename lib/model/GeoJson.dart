class GeoJson {
  String type;
  List<num> coordinates;

  GeoJson({ this.type, this.coordinates });

  factory GeoJson.fromJson(Map<String, dynamic> json) {
    return GeoJson(
      type: json['type'],
      coordinates: json['coordinates'].cast<num>()
    );
  }

  GeoJson.fromMap(Map<dynamic, dynamic> map) {
    this.type = map['type'];
    this.coordinates = map['coordinates'].cast<num>();
  }

  Map<String, dynamic> toJson() => {
    'type': type,
    'coordinates': coordinates
  };
}
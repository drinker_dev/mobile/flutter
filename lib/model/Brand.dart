class Brand {
  String _id;
  String _name;
  
  String get id => _id;
  String get name => _name;

  Brand.fromMap(Map<dynamic, dynamic> map) {
    this._id = map['_id'];
    this._name = map['name'];
  }
}
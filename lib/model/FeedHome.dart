import 'package:drinker/model/Brand.dart';
import 'package:drinker/model/Business.dart';
import 'package:drinker/model/product.dart';

class FeedHome {
  String _id;
  int _place;
  int _oldPlace;
  Business _business;
  Brand _brand;
  Product _product;
  num _price;
  DateTime _updatedAt;
  String _presentation;
 
  String get id => _id;
  int get place => _place;
  int get oldPlace => _oldPlace;
  num get price => _price;
  Product get product => _product;
  Business get business => _business;
  DateTime get updatedAt => _updatedAt;
  String get productName => '${_brand.name} ${_product.name}';
  Brand get brand => _brand;
  String get presentation => _presentation;
 
  FeedHome.fromMap(Map<dynamic, dynamic> map) {
    this._id = map['_id'];
    this._place = map['place'];
    this._oldPlace = map['oldPlace'];
    this._updatedAt = map['updatedAt'].toDate();
    this._business = Business.fromMap(map['business']);
    this._product = Product.fromMap(map['product']);
    this._brand = Brand.fromMap(map['brand']);
    this._price = map['price'];
    this._presentation = map['presentation'];
  }
}
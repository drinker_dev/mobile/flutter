import 'package:drinker/model/Brand.dart';
import 'package:drinker/model/product.dart';

class FeedProduct {
  Brand _brand;
  Product _product;
  String _presentation;
  num _average;
 
  String get id => _product.id;
  num get average => _average;
  Brand get brand => _brand;
  Product get product => _product;
  String get presentation => _presentation;
  String get name => '${_brand.name} ${_product.name}';
 
  FeedProduct.fromMap(Map<dynamic, dynamic> map) {

    this._brand = Brand.fromMap(map['brand']);
    this._product = Product.fromMap(map['product']);
    this._presentation = map['presentation'];
    this._average = map['average'];
  }

  FeedProduct.avg(List prices) {
    prices.fold(0, (prev, element) => prev + element);
  }
}
class Country {
  String id;
  String name;
  
  Country({this.id, this.name});

  factory Country.fromJson(Map<String, dynamic> json) {
    return Country(
      id: json['_id'] as String,
      name: json['name'] as String,
    );
  }

  Country.fromMap(Map<dynamic, dynamic> map) {
    this.id = map['_id'];
    this.name = map['name'];
  }
}
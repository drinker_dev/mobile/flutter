import 'package:drinker/model/Business.dart';
import 'package:drinker/model/product.dart';

class Home {
  String _id;
  int _place;
  int _oldPlace;
  num _price;
  Business _business;
  Product _product;
  DateTime _updatedAt;
 
  Home(this._id, this._business, this._product);
 
  Home.map(dynamic obj) {
    this._id = obj['id'];
  }
 
  String get id => _id;
  int get place => _place;
  int get oldPlace => _oldPlace;
  num get price => _price;
  Product get product => _product;
  Business get business => _business;
  DateTime get updatedAt => _updatedAt;
 
  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (_id != null) {
      map['id'] = _id;
    }
 
    return map;
  }
 
  Home.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._place = map['place'];
    this._oldPlace = map['oldPlace'];
    this._updatedAt = map['updated_at'].toDate();
    this._business = Business.fromMap(map['business']);
    this._product = Product.fromMap(map['product']);

  }
}
class Product {
  String id;
  String name;
  String presentation;
  String brand;
  num price;

  Product({ this.id, this.name, this.presentation, this.price, this.brand });

  String get nameComplete => "$brand $name";

  Product.fromMap(Map<dynamic, dynamic> map) {
    this.id = map['_id'];
    this.name = map['name'];
    this.price = map['price'];
  }
}
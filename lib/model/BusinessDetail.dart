import 'package:drinker/model/Business.dart';

class BusinessDetail {
  Business _business;
  num _price;

  Business get business => _business;
  String get id => _business.id;
  String get name => _business.name;
  num get price => _price;
 
  BusinessDetail.fromMap(Map<dynamic, dynamic> map) {
    this._business = Business.fromMap(map['_id']);
    this._price = map['price'];
  }
}
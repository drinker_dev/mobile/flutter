import 'package:drinker/model/Brand.dart';
import 'package:drinker/model/product.dart';

class ProductDeatail {
  Brand _brand;
  Product _product;
  String _presentation;
  num _price;

  Brand get brand => _brand;
  String get id => _product.id;
  String get name => '${_brand.name} ${_product.name}';
  String get presentation => _presentation;
  num get price => _price;

  ProductDeatail.fromMap(Map<dynamic, dynamic> map) {
    this._product = Product.fromMap(map['product']);
    this._brand = Brand.fromMap(map['brand']);
    this._presentation = map['presentation'];
    this._price = map['price'];
  }
}
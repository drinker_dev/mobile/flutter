class City {
  String id;
  String name;
  
  City({ this.id, this.name });

  factory City.fromJson(Map<String, dynamic> json) {
    return City(
      id: json['_id'] as String,
      name: json['name'] as String,
    );
  }

  City.fromMap(Map<dynamic, dynamic> map) {
    this.id = map['_id'];
    this.name = map['name'];
  }
}
import 'package:drinker/model/GeoJson.dart';

class Business {
  String id;
  String name;
  GeoJson geoLocation;
  String address;

  Business({ this.id, this.address, this.geoLocation, this.name });

  factory Business.fromJson(Map<dynamic, dynamic> json) {
    return Business(
      id: json['_id'] as String,
      name: json['name'] as String,
      geoLocation: json['geoLocation'] != null ? GeoJson.fromJson(json['geoLocation']) : new GeoJson(),
      address: json['address'] as String,
    );
  }

  Business.fromMap(Map<dynamic, dynamic> map) {
    this.id = map['_id'];
    this.name = map['name'];
    this.geoLocation = map['geoLocation'] != null ? GeoJson.fromMap(map['geoLocation']) : new GeoJson();
    this.address = map['address'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'address': address,
    'geoLocation': geoLocation.toJson()
  };
}
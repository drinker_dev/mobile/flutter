import 'package:drinker/model/GeoJson.dart';
import 'package:drinker/model/city.dart';
import 'package:drinker/model/country.dart';

class MapBusiness {
  String id;
  String name;
  GeoJson geoLocation;
  String address;
  Country country;
  City city;

  MapBusiness({ this.id, this.name, this.address, this.geoLocation, this.country, this.city });

  factory MapBusiness.fromJson(Map<dynamic, dynamic> json) {
    return MapBusiness(
      id: json['_id'] as String,
      name: json['name'] as String,
      geoLocation: GeoJson.fromJson(json['geoLocation']),
      address: json['address'] as String,
      country: json['country'] != null ? Country.fromMap(json['country']) : new Country(),
      city: json['city'] != null ? City.fromMap(json['city']) : new City()
    );
  }

  MapBusiness.fromMap(Map<dynamic, dynamic> map) {
    this.id = map['_id'];
    this.name = map['name'];
    this.geoLocation = GeoJson.fromMap(map['geoLocation']);
    this.address = map['address'];
    this.country = map['country'] == null ? Country.fromMap(map['country']) : new Country();
    this.city = map['city'] == null ? City.fromMap(map['city']) : new City();
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'geoLocation': geoLocation.toJson(),
    'address': address,
    'country': country,
    'city': city
  };
}
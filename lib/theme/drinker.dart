import 'package:flutter/material.dart';

ThemeData buildDrinkerTheme() {
  final ThemeData base = ThemeData.light();
  final Color textColor = Color.fromRGBO(46, 46, 46, 1);
  final Color fillColor = Color.fromRGBO(204, 204, 204, 1);
  final Color bottomBar = Color.fromRGBO(230, 230, 230, 1);
  final Color primary = Color.fromRGBO(194, 54, 65, 1);
  final Color accent = Color.fromRGBO(213, 134, 64, 1);
  final Color indicator = Color.fromRGBO(140, 140, 140, 1);
  return base.copyWith(
    primaryColor: primary,
    accentColor: accent,
    cursorColor: textColor,
    indicatorColor: indicator,
    inputDecorationTheme: InputDecorationTheme(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(4),
        borderSide: BorderSide(
          width: 0,
          style: BorderStyle.none,
        ),
      ),
      filled: true,
      fillColor: fillColor,
      hintStyle: TextStyle(
        fontFamily: 'Helvetica',
        fontSize: 18,
        fontWeight: FontWeight.w500,
        color: textColor
      ),
    ),
    textTheme: TextTheme(
      title: TextStyle(
        fontFamily: 'Helvetica',
        fontSize: 20,
        color: Colors.white,
        fontWeight: FontWeight.w500
      ),
      subtitle: TextStyle(
        fontFamily: 'Helvetica',
        fontSize: 16,
        color: Colors.white,
      ),
      body1: TextStyle(
        fontFamily: 'CaviarDreams',
        color: textColor,
        fontWeight: FontWeight.w700,
        fontSize: 14
      ),
      body2: TextStyle(
        fontFamily: 'CaviarDreams_Bold',
        color: textColor,
        fontWeight: FontWeight.w900,
        fontSize: 14
      )
    ),
    bottomAppBarTheme: BottomAppBarTheme(
      color: bottomBar
    ),
    accentTextTheme: TextTheme(
      body1: TextStyle(
        fontFamily: 'CaviarDreams',
        color: textColor,
        fontWeight: FontWeight.w700,
        fontSize: 16
      ),
      body2: TextStyle(
        fontFamily: 'CaviarDreams_Bold',
        color: textColor,
        fontWeight: FontWeight.w900,
        fontSize: 16
      )
    )
  );
}

import 'package:drinker/router/Router.dart';
import 'package:flutter/material.dart';
import 'package:drinker/theme/drinker.dart' as theme;
import 'package:flutter/services.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    statusBarBrightness: Brightness.light
  ));
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Drinker',
      theme: theme.buildDrinkerTheme(),
      initialRoute: '/',
      onGenerateRoute: Router.generateRoute,
    );
  }
}

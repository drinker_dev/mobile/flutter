import 'package:flutter/material.dart';

class AppBarCustom extends StatefulWidget implements PreferredSizeWidget {
  final double height;
  final String title;
  final String subTitle;
  final bool hasSeacrh;
  final bool hasSubTitle;
  final ValueChanged<String> onChanged;
  
  AppBarCustom({
    Key key,
    this.height = 90,
    this.title = '',
    this.subTitle = '',
    this.hasSeacrh = false,
    this.hasSubTitle = false,
    this.onChanged,
  }): super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(100);

  @override
  _AppBarCustomState createState() => _AppBarCustomState();
}

class _AppBarCustomState extends State<AppBarCustom> {

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(color: Colors.black26, spreadRadius: 3, blurRadius: 3)
            ]
          ),
          width: MediaQuery.of(context).size.width,
          height: widget.height,
          child: ClipRRect(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(4),
              bottomRight: Radius.circular(4)
            ),
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(194, 54, 65, 1),
                    Color.fromRGBO(204, 102, 71, 1),
                    Color.fromRGBO(213, 134, 64, 1)
                  ]
                ),
              ),
              child: Container(
                margin: EdgeInsets.fromLTRB(8, 20, 10, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    new Image(
                      image: new AssetImage('assets/icons/ic_menu_logo.png'),
                    ),
                    Expanded(
                      child: widget.hasSeacrh ? _inputSearch(widget.onChanged) : _boxTitle(widget.title, widget.subTitle),
                    ),
                  ],
                )
              )
            )
          ),
        ),
      ],
    );
  }

  Widget _boxTitle(String title, String subTitle) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        _widgetTitle(title),
        widget.hasSubTitle ? _widgetSubTitle(subTitle) : Container()
      ],
    );
  }

  Widget _widgetTitle(String title) {
    return Text(
      title,
      style: Theme.of(context).textTheme.title,
    );
  }

  Widget _widgetSubTitle(String subTitle) {
    return Text(
      subTitle,
      style: Theme.of(context).textTheme.subtitle
    );
  }

  Widget _inputSearch(onChanged) {
    return Container(
      margin: EdgeInsets.only(left: 5, top: 5),
      child: TextField(
        onChanged: onChanged,
        decoration: InputDecoration(
          hintText: 'Buscar',
          filled: true,
          fillColor: Colors.white,
          labelStyle: TextStyle(color: Colors.black),
          suffixIcon: Icon(Icons.search, color: Colors.black),
          contentPadding: EdgeInsets.only(top: 15.0, left: 10.0),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[400], width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            gapPadding: 5,
            borderSide: BorderSide(color: Colors.grey[400], width: 1),
          ),
        ),
        style: TextStyle(fontSize: 18.0),
      ),
    );
  }
}
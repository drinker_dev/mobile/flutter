import 'package:flutter/material.dart';

class AppSearch extends StatefulWidget {
  @override
  _AppSearchState createState() => _AppSearchState();
}

class _AppSearchState extends State<AppSearch> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0),
      child: TextField(
        decoration: InputDecoration(
          labelText: 'Buscar',
          hintText: 'Buscar',
          filled: true,
          fillColor: Colors.white,
          labelStyle: TextStyle(color: Colors.black),
          prefixIcon: Icon(Icons.search, color: Colors.black),
          contentPadding: EdgeInsets.all(5.0),
          border: InputBorder.none,
          focusedBorder: OutlineInputBorder(
            gapPadding: 5,
            borderSide: BorderSide(color: Colors.black, width: 1),
          ),
        ),
        style: TextStyle(
          fontSize: 18.0
        ),
      ),
    );
  }

  
}
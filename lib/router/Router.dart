import 'package:drinker/model/Business.dart';
import 'package:drinker/ui/business/business_detail.dart';
import 'package:flutter/material.dart';

import 'package:drinker/model/product.dart';
import 'package:drinker/ui/app_tabs.dart';
import 'package:drinker/ui/home/tab_home.dart';
import 'package:drinker/ui/map/tab_map.dart';
import 'package:drinker/ui/product/product_detail.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => AppTab());
      case '/home':
        return MaterialPageRoute(builder: (_) => TabHome());
      case '/product/detail':
        final Product product = settings.arguments;
        return MaterialPageRoute(builder: (_) => ProductDetail(product));
      case '/business/detail':
        final Business business = settings.arguments;
        return MaterialPageRoute(builder: (_) => BusinessDetail(business));
      case '/map/business':
        final Business business = settings.arguments;
        return MaterialPageRoute(builder: (_) => TabMap(business: business));
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}')
            ),
          )
        );
    }
  }
}